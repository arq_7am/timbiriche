/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singletons;

import objetosServicio.ControlJugadores;

/**
 *
 * @author Ald
 */
public class SingletonCtrlJugadores {
    private static ControlJugadores controlJugadores;
    
    private SingletonCtrlJugadores() {}
    
    public static ControlJugadores Instance() {
        if (controlJugadores == null) {
            controlJugadores = new ControlJugadores();
        }
        
        return controlJugadores;
    }
}
