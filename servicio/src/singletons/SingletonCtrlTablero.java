/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singletons;

import javax.swing.JPanel;
import objetosServicio.ControlTablero;

/**
 *
 * @author crisb
 */
public class SingletonCtrlTablero {
    private static ControlTablero controlTablero;
    
    private SingletonCtrlTablero() {}
    
    public static ControlTablero Instance(JPanel panel) {
        if (controlTablero == null) {
            controlTablero = new ControlTablero(panel);
        }
        
        return controlTablero;
    }
}
