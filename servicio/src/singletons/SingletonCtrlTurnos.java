/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singletons;

import objetosServicio.ControlJugadores;
import objetosServicio.ControlTurnos;

/**
 *
 * @author crisb
 */
public class SingletonCtrlTurnos {
    private static ControlTurnos controlTurnos;
    
    private SingletonCtrlTurnos() {}
    
    public static ControlTurnos Instance() {
        if (controlTurnos == null) {
            controlTurnos = new ControlTurnos();
        }
        
        return controlTurnos;
    }
}
