/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fachadas;

import dao.DAOException;
import java.awt.Color;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import objetosNegocio.Jugador;
import objetosNegocio.Sala;

/**
 *
 * @author crisb
 */
public interface IFachadaNegocio {

    //-------------Control Jugadores-------------------
    public void registrarJugador(String nombre, Icon icono) throws DAOException;

    public void eliminarJugador(Jugador jugador) throws DAOException;

    public void actualizarJugador(Jugador jugador) throws DAOException;

    public Jugador obtenerJugador(Jugador jugador) throws DAOException;

    public void actualizaColorJugador(Jugador jugador, Color color) throws DAOException;

    public ArrayList<Jugador> consultarJugadores();
    
    //------ Control Marcador------------------------
    public void añadirIndicadorPuntosJugador(JLabel indicadorPuntosJugador);

    public void añadirIndicadorColorJugador(JPanel inidcadorColorJugador);

    public void setIndicadorTurno(JLabel indicadorTurno);

    public JLabel getIndicadorTurno();

    public void actualizarPuntosMarcador();

    public void actualizarIndicadorTurno(String nombre);

    public void actualizarColoresMarcador();

    //-----------Control Salas----------------------
    public void crearSala(String nombre, int tamaño) throws Exception;

    public void registrarJugadorEnSala(String nombreSala, Jugador jugador);

    public ArrayList<Jugador> obtenerJugadoresEnSala(String nombreSala);

    public ArrayList<Sala> obtenerSalas();
    
    public void asignarColoresJugadores(String nombreSala);
    //---------------Control Tablero---------------
    public void dibujaTablero(int tamaño);
    public void establecerPanelTablero(JPanel panel);
    public void asignarFrameTablero(JFrame frameTablero);

    //-----------Control Turnos---------------------
    public Jugador obtenerJugadorEnTurno();
  
    public void asignarSiguiente();
    
    //----Varios----
      public void asignarNombreSala(String nombreSala);

 

}
