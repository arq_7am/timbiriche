/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fachadas;

import dao.DAOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import objetosNegocio.Jugador;
import objetosServicio.ControlJugadores;
import singletons.SingletonCtrlJugadores;

/**
 *
 * @author Ald
 */
public class FachadaCtrlJugadores implements IFachadaCtrlJugadores{

    ControlJugadores ctrlJugadores;
    
    public FachadaCtrlJugadores() {
        ctrlJugadores = SingletonCtrlJugadores.Instance();
    }
    
    @Override
    public void agregarJugador(Jugador jugador) {
        try {
            ctrlJugadores.registrarJugador(jugador.getNombre(), jugador.getIcono(), jugador.getColor());
        } catch (DAOException ex) {
            Logger.getLogger(FachadaCtrlJugadores.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void eliminarJugador(Jugador jugador) {
        try {
            ctrlJugadores.eliminarJugador(jugador);
        } catch (DAOException ex) {
            Logger.getLogger(FachadaCtrlJugadores.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void actualizarJugador(Jugador jugador) {
        try {
            ctrlJugadores.actualizarJugador(jugador);
        } catch (DAOException ex) {
            Logger.getLogger(FachadaCtrlJugadores.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Jugador obtener(Jugador jugador) {
        return ctrlJugadores.obtenerJugador(jugador);
    }

    @Override
    public ArrayList<Jugador> connsultarJugadores() {
        return ctrlJugadores.consultarJugadores();
    }
    
}
