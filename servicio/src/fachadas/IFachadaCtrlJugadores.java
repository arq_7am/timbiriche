/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fachadas;

import java.util.ArrayList;
import objetosNegocio.Jugador;

/**
 *
 * @author Ald
 */
public interface IFachadaCtrlJugadores {
    
    public void agregarJugador(Jugador jugador);
    public void eliminarJugador(Jugador jugador);
    public void actualizarJugador(Jugador jugador);
    public Jugador obtener(Jugador jugador);
    public ArrayList<Jugador> connsultarJugadores();
}
