/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fachadas;

import dao.DAOException;
import java.awt.Color;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import objetosNegocio.Jugador;
import objetosNegocio.Sala;
import objetosServicio.ControlJugadores;
import objetosServicio.ControlMarcador;
import objetosServicio.ControlSalas;
import objetosServicio.ControlTablero;
import objetosServicio.ControlTurnos;

/**
 *
 * @author crisb
 */
public class FachadaNegocio implements IFachadaNegocio {

    ControlJugadores controlJugadores;
    ControlMarcador controlMarcador;
    ControlSalas controlSalas;
    ControlTablero controlTablero;
    ControlTurnos controlTurnos;

    public FachadaNegocio() {
        try {
            controlJugadores = ControlJugadores.Instance();
            controlMarcador = ControlMarcador.Instance();
            controlSalas = ControlSalas.Instance();
            controlTablero = ControlTablero.Instance();
            controlTurnos = ControlTurnos.Instance();
        } catch (Exception e) {

        }

    }

    @Override
    public void registrarJugador(String nombre, Icon icono) throws DAOException {
        try {
            controlJugadores.registrarJugador(nombre, icono);
        } catch (DAOException e) {

        }
    }

    @Override
    public void eliminarJugador(Jugador jugador) throws DAOException {
        try {
            controlJugadores.eliminarJugador(jugador);
        } catch (DAOException e) {

        }
    }

    @Override
    public void actualizarJugador(Jugador jugador) throws DAOException {
        try {
            controlJugadores.actualizarJugador(jugador);
        } catch (DAOException e) {

        }
    }

    @Override
    public Jugador obtenerJugador(Jugador jugador) throws DAOException {

        return controlJugadores.obtenerJugador(jugador);

    }

    @Override
    public void actualizaColorJugador(Jugador jugador, Color color) throws DAOException {

        controlJugadores.actualizaColorJugador(jugador, color);

    }

    @Override
    public ArrayList<Jugador> consultarJugadores() {
        return controlJugadores.consultarJugadores();
    }

 

    @Override
     public void añadirIndicadorPuntosJugador(JLabel indicadorPuntosJugador) {
        controlMarcador.añadirIndicadorPuntosJugador(indicadorPuntosJugador);
    }

    @Override
    public void añadirIndicadorColorJugador(JPanel inidcadorColorJugador) {
        controlMarcador.añadirIndicadorColorJugador(inidcadorColorJugador);
    }

    @Override
    public void setIndicadorTurno(JLabel indicadorTurno) {
        controlMarcador.setIndicadorTurno(indicadorTurno);
    }

    @Override
    public JLabel getIndicadorTurno() {
        return controlMarcador.getIndicadorTurno();
    }

    @Override
    public void actualizarPuntosMarcador() {
        controlMarcador.actualizarPuntosMarcador();
    }

    @Override
    public void actualizarIndicadorTurno(String nombre) {
        controlMarcador.actualizarIndicadorTurno(nombre);
    }

    @Override
    public void actualizarColoresMarcador() {
        controlMarcador.actualizarColoresMarcador();
    }

    @Override
    public void crearSala(String nombre, int tamaño) throws Exception {
        controlSalas.crearSala(nombre, tamaño);
    }

    @Override
    public void registrarJugadorEnSala(String nombreSala, Jugador jugador) {
        controlSalas.registrarJugadorEnSala(nombreSala, jugador);
    }

    @Override
    public ArrayList<Jugador> obtenerJugadoresEnSala(String nombreSala) {
        return controlSalas.obtenerJugadoresEnSala(nombreSala);
    }

    @Override
    public ArrayList<Sala> obtenerSalas() {
        return controlSalas.obtenerSalas();
    }

    @Override
    public void dibujaTablero(int tamaño) {
        controlTablero.dibujaTablero(tamaño);
    }

    @Override
    public Jugador obtenerJugadorEnTurno() {
        return controlTurnos.obtenerJugadorEnTurno();
    }

    @Override
    public void asignarSiguiente() {
        controlTurnos.asignarSiguiente();
    }

    @Override
    public void establecerPanelTablero(JPanel panel) {
        controlTablero.establecerPanelTablero(panel);
    }

    @Override
    public void asignarColoresJugadores(String nombreSala) {
       controlSalas.asignarColoresJugadores(nombreSala);
    }

    @Override
    public void asignarNombreSala(String nombreSala) {
        controlMarcador.asignarNombreSala(nombreSala);
        controlTurnos.asignarNombreSala(nombreSala);
        controlMarcador.asignarNombreSala(nombreSala);
        controlTablero.asignarNombreSala(nombreSala);
        
    }

    @Override
    public void asignarFrameTablero(JFrame frameTablero) {
        controlTablero.asignarFrameTablero(frameTablero);
    }
    
}