/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetosServicio;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JLabel;
import javax.swing.JPanel;
import objetosNegocio.Jugador;
import observer.MarcadorEvent;
import observer.marcadorJuegoObserver;


/**
 *
 * @author crisb
 */
public class ControlMarcador {
    private ArrayList <JLabel> puntosJugadores;
    private ArrayList <JPanel> coloresJugadores;
    private ControlSalas controlSalas;
    private JLabel indicadorTurno;
    private String nombreSala;

    
   
    //----Patron singleton------------------
     private static ControlMarcador controlMarcador;
    
    private ControlMarcador() {
         Observer o1 = new marcadorJuegoObserver();
        this.getObservable().addObserver(o1);
        this.controlSalas=ControlSalas.Instance();
        this.puntosJugadores= new ArrayList <JLabel>();
        this.coloresJugadores= new ArrayList <JPanel>();
      
    }
    
    public static ControlMarcador Instance() {
        if (controlMarcador == null) {
            controlMarcador = new ControlMarcador();
        }
        
        return controlMarcador;
    }
    //--------------------------------------

    public void asignarNombreSala(String nombreSala){
        this.nombreSala=nombreSala;
    }
    public void añadirIndicadorPuntosJugador(JLabel indicadorPuntosJugador){
        this.puntosJugadores.add(indicadorPuntosJugador);
    }
    
    public void añadirIndicadorColorJugador(JPanel inidcadorColorJugador){
        this.coloresJugadores.add(inidcadorColorJugador);
    }
    public void setIndicadorTurno(JLabel indicadorTurno){
        this.indicadorTurno=indicadorTurno;
       this.indicadorTurno.setText(controlSalas.obtenerJugadoresEnSala(nombreSala).get(0).getNombre());
    }
    
    public JLabel getIndicadorTurno(){
        return indicadorTurno;
    }
    
    
    
    public void actualizarIndicadorTurno(String nombre){
        indicadorTurno.setText(nombre);
    }
    
    
    public void actualizarColoresMarcador(){
        for (int i = 0; i < coloresJugadores.size(); i++) {
            coloresJugadores.get(i).setBackground(controlSalas.obtenerJugadoresEnSala(nombreSala).get(i).getColor());
        }
    }
    
    public Jugador obtenerGanador(String nombreSala){
        int puntuacionMayor=0;
        Jugador jugadorGanador=null;
        
        for (int i = 0; i < controlSalas.obtenerJugadoresEnSala(nombreSala).size(); i++) {
            Jugador jugadorActual=controlSalas.obtenerJugadoresEnSala(nombreSala).get(i);
            if( jugadorActual.getPuntuacion()>puntuacionMayor){
               puntuacionMayor=jugadorActual.getPuntuacion();
               jugadorGanador=jugadorActual;
            }
        }
        return jugadorGanador;
    }
    
     //---------Patron Observer Para la Actualizacion de Puntos del Marcador--------
     public void actualizarPuntosMarcador() {
        MarcadorEvent evento = new MarcadorEvent(puntosJugadores, controlSalas.obtenerJugadoresEnSala(nombreSala));
//        for (int i = 0; i < puntosJugadores.size(); i++) {
//            puntosJugadores.get(i).setText(String.valueOf(controlSalas.obtenerJugadoresEnSala(nombreSala).get(i).getPuntuacion())); 
//        }
        synchronized (OBSERVABLE) {
            OBSERVABLE.setChanged();
            OBSERVABLE.notifyObservers(evento);
        }
    }
    private static final MarcadorObservable OBSERVABLE;
     static {
        OBSERVABLE = new MarcadorObservable();
    }

    public static Observable getObservable() {
        return OBSERVABLE;
    }
   
     private static class MarcadorObservable extends Observable {
        @Override
        public synchronized void setChanged() {
            super.setChanged();
        }
    }
  //------------------------------------
}
