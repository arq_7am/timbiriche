/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetosServicio;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;
import objetosNegocio.Jugador;
import objetosNegocio.Sala;

/**
 *
 * @author crisb
 */
public class ControlSalas {
  
    private ArrayList <Sala> salas;
    private static ControlSalas controlSala;
    
    //---------Patrón Singleton---------
    private ControlSalas() {
     this.salas= new ArrayList <Sala>();
    }
    
    public static ControlSalas Instance() {
        if (controlSala == null) {
            controlSala = new ControlSalas();
        }
        
        return controlSala;
    }
   //--------------------------------------
    
    public void crearSala(String nombre, int tamaño) throws Exception{
       if(obtenerSala(nombre)==null){
           Sala sala = new Sala (nombre, tamaño);
           salas.add(sala);
       } else{
          throw new Exception();
       }      
    }
    
    public void registrarJugadorEnSala(String nombreSala,Jugador jugador){
        Sala salaActual= obtenerSala(nombreSala);
        salaActual.añadirJuagdorEnSala(jugador);
    }
    
    public ArrayList <Jugador> obtenerJugadoresEnSala(String nombreSala){
        Sala salaActual= obtenerSala(nombreSala);
        return salaActual.getJugadoresSala();
    }
    
    public Jugador obtenerJugadorEnSala(Jugador jugador, String nombreSala){
        for (int i = 0; i < obtenerJugadoresEnSala(nombreSala).size(); i++) {
            if(jugador.equals(obtenerJugadoresEnSala(nombreSala).get(i))){
                return obtenerJugadoresEnSala(nombreSala).get(i);
            }
        }
        return null;
    }
    
    private Sala obtenerSala(String nombreSala){
        for (int i = 0; i < salas.size(); i++) {
            if(nombreSala.equalsIgnoreCase(salas.get(i).getNombre())){    
                return salas.get(i);
            }
        }
        return null;
    }
    
    public ArrayList <Sala> obtenerSalas(){
        return salas;
    }
    
    private Color randomColor() {
        Random random = new Random();
        float r = random.nextFloat();
        float g = random.nextFloat();
        float b = random.nextFloat();
        
        Color color = new Color(r, g, b);
        
        return color;
    }
    
    public void asignarColoresJugadores(String nombreSala) {
        for (int i = 0; i < obtenerJugadoresEnSala(nombreSala).size(); i++) {
            Color colorAleatorio = randomColor();
            obtenerJugadoresEnSala(nombreSala).get(i).setColor(colorAleatorio);
        }
    }
    
    
    
}
