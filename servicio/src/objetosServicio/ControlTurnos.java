/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetosServicio;

import java.util.ArrayList;
import objetosNegocio.Jugador;
import objetosNegocio.Turno;

/**
 *
 * @author lv1821
 */



public class ControlTurnos {
 
    private ControlSalas controlSalas;
    String nombreSala;
    private int turnoSig;
    private Turno turno;

    //-------------------Patron singleton----------
    private static ControlTurnos controlTurnos;

    private ControlTurnos() {
        this.controlSalas= ControlSalas.Instance();
        this.turno = new Turno();
     
    }

    private void asignarPrimerTurno(){
        turno.setJugador(controlSalas.obtenerJugadoresEnSala(nombreSala).get(0));
   }
    
    public void asignarNombreSala(String nombreSala){
        this.nombreSala=nombreSala;
        asignarPrimerTurno();
    }

    public static ControlTurnos Instance() {
        if (controlTurnos == null) {
            controlTurnos = new ControlTurnos();
        }
        
        return controlTurnos;
    }
    
    //---------------------------------

    public Jugador obtenerJugadorEnTurno() {
        return turno.getJugador();
    }

    public void asignarSiguiente() {
        turnoSig = turnoSig + 1;
        if (turnoSig > controlSalas.obtenerJugadoresEnSala(nombreSala).size() - 1) {
            turnoSig = 0;
        }
        turno.setJugador(controlSalas.obtenerJugadoresEnSala(nombreSala).get(turnoSig));
    }
}
