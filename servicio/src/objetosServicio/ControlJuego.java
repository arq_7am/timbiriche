/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetosServicio;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
<<<<<<< HEAD
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
=======
>>>>>>> 44d1ccdb68acd3d3702942bd35cb22ac4a007d6e
import objetosNegocio.Cuadro;
import objetosNegocio.Linea;

/**
 * @author crisb
 */
public class ControlJuego implements ActionListener, Runnable {

    Linea linea;
    Color color;
    Cuadro cuadro;
    String ip;
    ArrayList<Cuadro> cuadrosTablero;
    ArrayList<Linea> lineasTablero;
    ControlTurnos controlTurnos;
    ControlJugadores controlJugadores;
    ControlMarcador controlMarcador;

    public ControlJuego(Linea linea, ArrayList<Cuadro> cuadrosTablero, ArrayList<Linea> lineasTablero,ArrayList<JPanel> marcadoresJugadores) throws UnknownHostException {
    public ControlJuego(Linea linea, ArrayList<Cuadro> cuadrosTablero, ArrayList<Linea> lineasTablero) {
        this.linea = linea;
        this.lineasTablero = lineasTablero;
        this.cuadrosTablero = cuadrosTablero;
        this.lineasTablero = lineasTablero;
        this.controlTurnos = SingletonCtrlTurnos.Instance();
        this.marcadoresJugadores=marcadoresJugadores;
        ip= InetAddress.getLocalHost().getHostAddress();
        Thread miHilo = new Thread(this);
        miHilo.start();
        this.controlTurnos = ControlTurnos.Instance();
        this.controlMarcador = ControlMarcador.Instance();
        this.controlJugadores = ControlJugadores.Instance();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (linea.isCompletado() == false) {
            pintarLinea(linea);
            try {
                Socket envio = new Socket(ip, 9000);

                Socket envio = new Socket("192.168.0.11", 9000);
                try (DataOutputStream identificador = new DataOutputStream(envio.getOutputStream())) {
                    identificador.writeInt(linea.getId());
                }
            } catch (IOException ex) {
                System.out.println("Falla envio de dato");
            }

            linea.setCompletado(true);
            comprobarCuadrosPintados();
            controlMarcador.actualizarPuntosMarcador();
            ///asigna el turno al siguiente jugador
        }
        controlMarcador.actualizarIndicadorTurno(controlTurnos.obtenerJugadorEnTurno().getNombre());
    }

    public void comprobarCuadrosPintados() {
        for (int i = 0; i < cuadrosTablero.size(); i++) {
            this.cuadro = cuadrosTablero.get(i);
            if (cuadro.isCompletado() == false) {
                if (cuadro.getSuperior().isCompletado()
                        && cuadro.getInferior().isCompletado()
                        && cuadro.getDerecha().isCompletado()
                        && cuadro.getIzquierda().isCompletado()) {
                    cuadro.setColor(color);
                    controlJugadores.añadirPunto(controlTurnos.obtenerJugadorEnTurno());
                    cuadro.setCompletado(true);
                    comprobarJuegoTerminado();

                }
            }
        }

    }

    private void comprobarJuegoTerminado() {
        int sumita = 0;
        for (int i = 0; i < controlJugadores.consultarJugadores().size(); i++) {
            sumita = sumita + controlJugadores.consultarJugadores().get(i).getPuntuacion();
        }
        if (sumita == 100) {
            System.out.println("GAME OVER");
        }
    }

    public void pintarLinea(Linea lineaPintar) {
        color = controlTurnos.obtenerJugadorEnTurno().getColor();
        lineaPintar.setBackground(color);
        lineaPintar.setCompletado(true);
        comprobarCuadrosPintados();
        controlTurnos.asignarSiguiente();
    }

    public void pintarLineaRemota(int identificador) {
        for (Linea lineaRemota: lineasTablero){
            if(lineaRemota.getId()==identificador){
                pintarLinea(lineaRemota);
            }
        }
    }

    @Override
    public void run() {
        try {
            
            ServerSocket servidor = new ServerSocket(9991);
            
            while(true){
                
                Socket miSocket = servidor.accept();
                
                DataInputStream entrada = new DataInputStream(miSocket.getInputStream());
                
                int identificador = entrada.readInt();
                
                pintarLineaRemota(identificador);
                
            }
            
        } catch (IOException ex) {
            Logger.getLogger(ControlJuego.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
