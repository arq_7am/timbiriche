package objetosServicio;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import objetosNegocio.Cuadro;
import objetosNegocio.Linea;
import objetosNegocio.Tablero;

/**
 *
 * @author crisb
 */
public class ControlTablero  {
    private Tablero tablero;
    private ArrayList <Linea> lineasTablero;
    private ArrayList <Cuadro> cuadrosTablero;
    private JFrame frameTablero;
    private JPanel panelTablero;
    private String nombreSala;
    private int tamaño;
    
//-----------Patron Singleton-------------
    private static ControlTablero controlTablero;

    private ControlTablero() {
        tablero = new Tablero();
        lineasTablero = tablero.getLineasTablero();
        cuadrosTablero = tablero.getCuadrosTablero();
//        this.panelTablero = panel;
    }

    public static ControlTablero Instance() {
        if (controlTablero == null) {
            controlTablero = new ControlTablero();
        }

        return controlTablero;
    }
  //-----------------------------------------
   
    public void establecerPanelTablero(JPanel panel){
        this.panelTablero=panel;
    }
    public void dibujaTablero(int tamaño){
        this.tamaño=tamaño;
        tablero.dibujar(this.panelTablero,this.tamaño);  
        panelTablero.repaint();
        crearAccionesTablero();
    }
    
    private void crearAccionesTablero() {
        for (int i = 0; i < lineasTablero.size(); i++) {
            Linea linea = lineasTablero.get(i);
            linea.addActionListener(new ControlJuego(linea, cuadrosTablero,lineasTablero,nombreSala,frameTablero,tamaño));
        }

    }
   public void asignarNombreSala(String nombreSala){
        this.nombreSala=nombreSala;
    }
   
   public void asignarFrameTablero(JFrame frameTablero){
       this.frameTablero=frameTablero;
   }



   
}
