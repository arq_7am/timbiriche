/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetosServicio;

import dao.DAOException;
import dao.DAOJugador;
import java.awt.Color;
import java.util.ArrayList;
import javax.swing.Icon;
import objetosNegocio.Jugador;

/**
 *
 * @author crisb
 */
public class ControlJugadores {
    
    private DAOJugador dj = new DAOJugador();
    //----- PATRON SINGLETON------
    private static ControlJugadores controlJugadores;
    
    private ControlJugadores() {}
    
    public static ControlJugadores Instance() {
        if (controlJugadores == null) {
            controlJugadores = new ControlJugadores();
        }
        
        return controlJugadores;
    }
    //-----------------------------
    public void registrarJugador(String nombre, Icon icono) throws DAOException{
       
       dj.agregarJugador(new Jugador(nombre, icono, 0));
    }
    
    public void eliminarJugador(Jugador jugador) throws DAOException {
       dj.eliminarJugador(jugador);
    }
    
    public void actualizarJugador(Jugador jugador) throws DAOException {
        dj.actualizarJugador(jugador);
    }
    
    public Jugador obtenerJugador(Jugador jugador) {
        return dj.obtener(jugador);
    }
    
    public void actualizaColorJugador(Jugador jugador,Color color){
        jugador.setColor(color);
    }
    
    public ArrayList<Jugador> consultarJugadores() {
        return dj.obtenerTodos();
    }
   

}
