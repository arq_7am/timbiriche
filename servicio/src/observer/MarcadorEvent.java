/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

import java.util.ArrayList;
import javax.swing.JLabel;
import objetosNegocio.Jugador;

/**
 *
 * @author crisb
 */
public class MarcadorEvent {
   ArrayList <JLabel> puntosJugadores;
   ArrayList<Jugador> jugadoresEnSala;
    public MarcadorEvent(ArrayList <JLabel> puntosJugadores, ArrayList<Jugador> jugadoresEnSala){
        this.puntosJugadores=puntosJugadores;
        this.jugadoresEnSala=jugadoresEnSala;
    }
    public void actualizarMarcador(){
         for (int i = 0; i < puntosJugadores.size(); i++) {
            puntosJugadores.get(i).setText(String.valueOf(jugadoresEnSala.get(i).getPuntuacion())); 
        }
    }
}
