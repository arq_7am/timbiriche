/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author crisb
 */
public class marcadorJuegoObserver implements Observer {

    @Override
    public void update(Observable o, Object args) {
        if (args instanceof MarcadorEvent) {
            MarcadorEvent evento = (MarcadorEvent) args;
            evento.actualizarMarcador();
        }
    }
    
}
