/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singletons;

import presentacion.FrmMenuPrincipal;

/**
 *
 * @author Ald
 */
public class SingletonFrmMenuP {
    private static FrmMenuPrincipal frmMenuP;
    
    private SingletonFrmMenuP () {}
    
    public static FrmMenuPrincipal instance() {
        if (frmMenuP == null) {
            frmMenuP = new FrmMenuPrincipal();
        }
        
        return frmMenuP;
    }
}
