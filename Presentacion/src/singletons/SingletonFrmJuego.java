/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singletons;

import presentacion.FrmJuego;

/**
 *
 * @author Ald
 */
public class SingletonFrmJuego {
    
    private static FrmJuego frmJuego;
    
    private SingletonFrmJuego () {}
    
    public static FrmJuego instance() {
        if (frmJuego == null) {
            frmJuego = new FrmJuego();
        }
        
        return frmJuego;
    }
}
