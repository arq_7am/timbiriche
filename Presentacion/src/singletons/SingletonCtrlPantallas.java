/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singletons;

import presentacion.ControlPantallas;

/**
 *
 * @author Ald
 */
public class SingletonCtrlPantallas {
    private static ControlPantallas controlPantallas;
    
    private SingletonCtrlPantallas() {}
    
    public static ControlPantallas Instance() {
        if (controlPantallas == null) {
            controlPantallas = new ControlPantallas();
        }
        
        return controlPantallas;
    }
}
