/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import presentacion.FrmJuego;
import presentacion.FrmMenuPrincipal;
import singletons.SingletonFrmJuego;
import singletons.SingletonFrmMenuP;

/**
 *
 * @author Ald
 */
public class ControlPantallas {
    
    public ControlPantallas() {
        
    }
    
    public void mostrarPantallaMenuPrincipal() {
        FrmMenuPrincipal frmMenuP = SingletonFrmMenuP.instance();
        frmMenuP.setVisible(true);
    }
    
    public void mostrarPantallaJuego() {
        FrmJuego frmJuego = SingletonFrmJuego.instance();
        frmJuego.setVisible(true);
    }
    
}
