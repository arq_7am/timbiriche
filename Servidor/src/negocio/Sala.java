/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import objetosNegocio.Jugador;

/**
 *
 * @author JearPC
 */
public class Sala {
    String ip;
    Jugador lider;
    ArrayList<Jugador> jugadores;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Jugador getLider() {
        return lider;
    }

    public void setLider(Jugador lider) {
        this.lider = lider;
    }

    public ArrayList<Jugador> getJugadores() {
        return jugadores;
    }

    public void anadirJugadores(Jugador jugador) {
        jugadores.add(jugador);
    }
    
    public void eliminarJugador(Jugador jugador){
        jugadores.remove(jugador);
    }
}
