package objetosNegocio;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Color;
import javax.swing.JButton;

/**
 *
 * @author crisb
 */
public class Cuadro extends JButton {

    
    private Color color;
    private int xposicion;
    private int yposicion;
    private int ancho;
    private int alto;
    private Linea superior;
    private Linea inferior;
    private Linea derecha;
    private Linea izquierda;
    private boolean completado;
    
    public Cuadro(Color color, int xposicion, int yposicion, int ancho, int alto){
        super.setBackground(color);
        super.setBounds(xposicion, yposicion, ancho, alto);
        this.completado=false;
        //super.setEnabled(false);
    }
   
    public Cuadro( Color color, int xposicion, int yposicion, int ancho, int alto, Linea superior, Linea inferior, Linea derecha, Linea izquierda){
        //super.setEnabled(false);
          super.setBackground(color);
        super.setBounds(xposicion, yposicion, ancho, alto);
        this.superior=superior;
        this.inferior=inferior;
        this.derecha=derecha;
        this.izquierda=izquierda;
        this.completado=false;
    }
    
    public void setLineas(Linea superior, Linea inferior, Linea derecha, Linea izquierda){
        this.superior=superior;
        this.inferior=inferior;
        this.derecha=derecha;
        this.izquierda=izquierda;
    }
    
    public void setColor(Color color){
        super.setBackground(color);
    }
    
  

    public int getXposicion() {
        return xposicion;
    }

    public void setXposicion(int xposicion) {
        this.xposicion = xposicion;
    }

    public int getYposicion() {
        return yposicion;
    }

    public void setYposicion(int yposicion) {
        this.yposicion = yposicion;
    }

    public int getAncho() {
        return ancho;
    }

    public void setAncho(int ancho) {
        this.ancho = ancho;
    }

    public int getAlto() {
        return alto;
    }

    public void setAlto(int alto) {
        this.alto = alto;
    }

    public Linea getSuperior() {
        return superior;
    }

    public void setSuperior(Linea superior) {
        this.superior = superior;
    }

    public Linea getInferior() {
        return inferior;
    }

    public void setInferior(Linea inferior) {
        this.inferior = inferior;
    }

    public Linea getDerecha() {
        return derecha;
    }

    public void setDerecha(Linea derecha) {
        this.derecha = derecha;
    }

    public Linea getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(Linea izquierda) {
        this.izquierda = izquierda;
    }

    public boolean isCompletado() {
        return completado;
    }

    public void setCompletado(boolean completado) {
        this.completado = completado;
    }

    @Override
    public String toString() {
        return "Cuadro{" + "color=" + color + ", superior=" + superior + ", inferior=" + inferior + ", derecha=" + derecha + ", izquierda=" + izquierda + '}';
    }
    
    
}
