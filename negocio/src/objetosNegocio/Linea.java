/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetosNegocio;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 *
 * @author crisb
 */
public class Linea extends JButton {

    private Color color;
    private int xposicion;
    private int yposicion;
    private int ancho;
    private int alto;
    private int id;
    private boolean completado;

    public Linea(Color color, int xposicion, int yposicion, int ancho, int alto, int id) {
        super.setBackground(color);
        super.setBounds(xposicion, yposicion, ancho, alto);
        this.color = color;
        this.xposicion = xposicion;
        this.yposicion = yposicion;
        this.id=id;
    }
    
    public Linea(int xposicion, int yposicion){
        super.setBounds(xposicion, yposicion, TOP, TOP);
        this.xposicion=xposicion;
        this.yposicion=yposicion;
    }

    public Color getColor() {
        return super.getBackground();
    }

    public void setColor(Color color) {
        super.setBackground(color);
    }

    public int getXposicion() {
        return xposicion;
    }

    public void setXposicion(int xposicion) {
        this.xposicion = xposicion;
    }

    public int getYposicion() {
        return yposicion;
    }

    public void setYposicion(int yposicion) {
        this.yposicion = yposicion;
    }

    public int getAncho() {
        return ancho;
    }

    public void setAncho(int ancho) {
        this.ancho = ancho;
    }

    public int getAlto() {
        return alto;
    }

    public void setAlto(int alto) {
        this.alto = alto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isCompletado() {
        return completado;
    }

    public void setCompletado(boolean completado) {
        this.completado = completado;
    }

    @Override
    public String toString() {
        return "Linea{" + "color=" + color + ", xposicion=" + xposicion + ", yposicion=" + yposicion + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Linea other = (Linea) obj;
        if (this.xposicion != other.xposicion) {
            return false;
        }
        if (this.yposicion != other.yposicion) {
            return false;
        }
        return true;
    }

}
