package objetosNegocio;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author crisb
 */
public class Tablero {

    private ArrayList<Linea> lineasHorizontales;
    private ArrayList<Linea> lineasVerticales;
    private ArrayList<Linea> lineas;
    private ArrayList<Cuadro> cuadros;
    private ArrayList<JButton> elementosTablero;
    private int tamaño;
    private int idLinea;

    public Tablero() {
        this.lineasHorizontales = new ArrayList<Linea>();
        this.lineasVerticales = new ArrayList<Linea>();
        this.cuadros = new ArrayList<Cuadro>();
        this.lineas = new ArrayList<Linea>();
        this.elementosTablero = new ArrayList<JButton>();
    }

    public void dibujar(JPanel panel, int tamaño) {
        this.tamaño = tamaño;
        creaElementos();
        for (int i = 0; i < elementosTablero.size(); i++) {
            panel.add(elementosTablero.get(i));
        }

    }

    public ArrayList<Linea> getLineasTablero() {
        return lineas;
    }

    public ArrayList<Cuadro> getCuadrosTablero() {
        return cuadros;
    }

    private void creaElementos() {
        dibujarLineasHorizontales();
        dibujarLineasVerticales();
        dibujarCuadros();
    }

    private void dibujarCuadros() {
        int xposicion = 15;
        int yposicion = 45;
        int ancho = 35;
        int alto = 35;
//con el contador x sabremos cuando terminamos de dibujar una fila
        int contadorX = 0;
        for (int i = 0; i < (tamaño) * (tamaño); i++) {
            int cont = i + 1;
            //si el contador de x es 20, es decir, se termino una fila, aumentamos la posicion en y
// para crear una nueva fila, reiniciamos la posicion x y el contador de x
            if (contadorX == (tamaño)) {
                yposicion = yposicion + 50;
                xposicion = 15;
                contadorX = 0;
            }
//añadimos el cuadrado a la lista

            Linea superior = new Linea(xposicion, yposicion - 15);
            Linea inferior = new Linea(xposicion, yposicion + 35);
            Linea derecha = new Linea(xposicion + 35, yposicion);
            Linea izquierda = new Linea(xposicion - 15, yposicion);

            for (int j = 0; j < lineas.size(); j++) {
                if (lineas.get(j).equals(superior)) {
                    superior = lineas.get(j);
                }
                if (lineas.get(j).equals(inferior)) {
                    inferior = lineas.get(j);
                }
                if (lineas.get(j).equals(derecha)) {
                    derecha = lineas.get(j);
                }
                if (lineas.get(j).equals(izquierda)) {
                    izquierda = lineas.get(j);
                }

            }
            cuadros.add(new Cuadro(Color.WHITE, xposicion, yposicion, ancho, alto, superior, inferior, derecha, izquierda));

            elementosTablero.add(cuadros.get(i));
//añadimos 30 de distancia en x entre cada coordenada de los cuadros
            xposicion = xposicion + 50;
            contadorX++;
        }
    }

    private void dibujarLineasHorizontales() {
        int xposicion = 15;
        int yposicion = 30;
        int ancho = 35;
        int alto = 15;
//con el contador x sabremos cuando terminamos de dibujar una fila
        int contadorX = 0;
        for (int i = 0; i < (tamaño * tamaño) + (tamaño); i++) {
            //si el contador de x es 20, es decir, se termino una fila, aumentamos la posicion en y
// para crear una nueva fila, reiniciamos la posicion x y el contador de x
            if (contadorX == tamaño) {
                yposicion = yposicion + 50;
                xposicion = 15;
                contadorX = 0;
            }
//añadimos el cuadrado a la lista
            lineasHorizontales.add(new Linea(Color.WHITE, xposicion, yposicion, ancho, alto, idLinea));
            lineas.add(lineasHorizontales.get(i));
            elementosTablero.add(lineasHorizontales.get(i));

//añadimos 30 de distancia en x entre cada coordenada de los cuadros
            xposicion = xposicion + 50;
            contadorX++;
            idLinea++;
        }
    }

    private void dibujarLineasVerticales() {
        int xposicion = 0;
        int yposicion = 45;
        int ancho = 15;
        int alto = 40;
//con el contador x sabremos cuando terminamos de dibujar una fila
        int contadorX = 0;
        for (int i = 0; i < (tamaño * tamaño) + (tamaño); i++) {
            //si el contador de x es 20, es decir, se termino una fila, aumentamos la posicion en y
// para crear una nueva fila, reiniciamos la posicion x y el contador de x
            if (contadorX == tamaño + 1) {
                yposicion = yposicion + 50;
                xposicion = 0;
                contadorX = 0;
            }
//añadimos el cuadrado a la lista
            lineasVerticales.add(new Linea(Color.WHITE, xposicion, yposicion, ancho, alto, idLinea));
            lineas.add(lineasVerticales.get(i));
            elementosTablero.add(lineasVerticales.get(i));

//añadimos 30 de distancia en x entre cada coordenada de los cuadros
            xposicion = xposicion + 50;
            contadorX++;
            idLinea++;
        }
    }

}
