/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetosNegocio;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author crisb
 */
public class Sala {
    private String nombreSala;
    private ArrayList <Jugador> jugadoresSala;
    private int tamañoTablero;
    
    
   public Sala(String nombreSala, int tamañoTablero){
      this.nombreSala=nombreSala;
      this.jugadoresSala= new ArrayList <Jugador>();
      this.tamañoTablero=tamañoTablero; 
   }
   
   public int getTamañoTablero(){
       return tamañoTablero;
   }
   public String getNombre(){
       return nombreSala;
   }
   public void añadirJuagdorEnSala(Jugador jugador){
       this.jugadoresSala.add(jugador);
   }
   
   public ArrayList <Jugador> getJugadoresSala(){
       return jugadoresSala;
   }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.nombreSala);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sala other = (Sala) obj;
        if (!Objects.equals(this.nombreSala, other.nombreSala)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Sala{" + "nombreSala=" + nombreSala + ", tama\u00f1oTablero=" + tamañoTablero + '}';
    }
   
    
   
}
