/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.awt.List;
import java.util.ArrayList;
import java.util.Iterator;
import objetosNegocio.Jugador;

/**
 *
 * @author Ald
 */
public class DAOJugador<T> extends DAOBase<Jugador> {

    private ArrayList<Jugador> jugadores;

    public DAOJugador() {
        jugadores = new ArrayList();
    }

    @Override
    public void agregarJugador(Jugador entidad) {
        jugadores.add(entidad);
    }

    @Override
    public void eliminarJugador(Jugador entidad) throws DAOException {
        Jugador nuevoJugador = obtener(entidad);
        if (nuevoJugador != null) {
            this.jugadores.remove(obtener(entidad));
        } else {
            throw new DAOException("No se encontró el jugador");
        }
    }

    @Override
    public void actualizarJugador(Jugador entidad) throws DAOException {
        Jugador nuevoJugador = obtener(entidad);
        if (nuevoJugador != null) {
            this.jugadores.set(jugadores.indexOf(nuevoJugador), entidad);
        } else {
            throw new DAOException("No se encontró el jugador");
        }
    }

    @Override
    public Jugador obtener(Jugador entidad) {
        Iterator<Jugador> itJugador = this.jugadores.iterator();
        while (itJugador.hasNext()) {
            Jugador nuevoJugador = itJugador.next();
            if (nuevoJugador.equals(entidad)) {
                return nuevoJugador;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Jugador> obtenerTodos() {
        return jugadores;
    }

}
