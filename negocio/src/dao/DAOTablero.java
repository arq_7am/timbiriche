/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import java.util.Iterator;
import objetosNegocio.Tablero;

/**
 *
 * @author Ald
 */
public class DAOTablero<T> extends DAOBase<Tablero> {

    private ArrayList<Tablero> tableros;
    
    public DAOTablero() {
        tableros = new ArrayList();
    }
    
    @Override
    public void agregarJugador(Tablero entidad) {
        tableros.add(entidad);
    }

    @Override
    public void eliminarJugador(Tablero entidad) throws DAOException {
        Tablero nuevotTablero = obtener(entidad);
        if (nuevotTablero != null) {
            this.tableros.remove(obtener(entidad));
        } else {
            throw new DAOException("No se encontró el tablero");
        }
    }

    @Override
    public void actualizarJugador(Tablero entidad) throws DAOException {
        Tablero nuevoTablero = obtener(entidad);
        if (nuevoTablero != null) {
            this.tableros.set(tableros.indexOf(nuevoTablero), entidad);
        } else {
            throw new DAOException("No se encontró el tablero");
        }
    }

    @Override
    public Tablero obtener(Tablero entidad) throws DAOException {
        Iterator<Tablero> itTablero = this.tableros.iterator();
        while (itTablero.hasNext()) {
            Tablero nuevoTablero = itTablero.next();
            if (nuevoTablero.equals(entidad)) {
                return nuevoTablero;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Tablero> obtenerTodos() {
        return tableros;
    }
    
}
