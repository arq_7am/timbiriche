/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import java.util.Iterator;
import objetosNegocio.Linea;

/**
 *
 * @author Ald
 */
public class DAOLinea<T> extends DAOBase<Linea> {

    private ArrayList<Linea> lineas;
    
    public DAOLinea() {
        lineas = new ArrayList();
    }
    
    @Override
    public void agregarJugador(Linea entidad) {
        lineas.add(entidad);
    }

    @Override
    public void eliminarJugador(Linea entidad) throws DAOException {
        Linea nuevaLinea = obtener(entidad);
        if (nuevaLinea != null) {
            this.lineas.remove(obtener(entidad));
        } else {
            throw new DAOException("No se encontró la línea");
        }
    }

    @Override
    public void actualizarJugador(Linea entidad) throws DAOException {
        Linea nuevaLinea = obtener(entidad);
        if (nuevaLinea != null) {
            this.lineas.set(lineas.indexOf(nuevaLinea), entidad);
        } else {
            throw new DAOException("No se encontró la línea");
        }
    }

    @Override
    public Linea obtener(Linea entidad) throws DAOException {
        Iterator<Linea> itLinea = this.lineas.iterator();
        while (itLinea.hasNext()) {
            Linea nuevaLinea = itLinea.next();
            if (nuevaLinea.equals(entidad)) {
                return nuevaLinea;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Linea> obtenerTodos() {
        return lineas;
    }
    
}
