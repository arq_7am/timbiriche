/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.awt.List;
import java.util.ArrayList;

/**
 *
 * @author Ald
 * @param <T>
 */
public abstract class DAOBase<T> {
    public abstract void agregarJugador(T entidad);
    public abstract void eliminarJugador(T entidad) throws DAOException;
    public abstract void actualizarJugador(T entidad) throws DAOException;
    public abstract T obtener(T entidad) throws DAOException;
    public abstract ArrayList<T> obtenerTodos();
}
