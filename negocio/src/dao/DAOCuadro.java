/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import java.util.Iterator;
import objetosNegocio.Cuadro;
import objetosNegocio.Jugador;

/**
 *
 * @author Ald
 */
public class DAOCuadro<T> extends DAOBase<Cuadro> {

    private ArrayList<Cuadro> cuadros;
    
    public DAOCuadro() {
        cuadros = new ArrayList();
    }

    @Override
    public void agregarJugador(Cuadro entidad) {
        cuadros.add(entidad);
    }

    @Override
    public void eliminarJugador(Cuadro entidad) throws DAOException {
        Cuadro nuevoCuadro = obtener(entidad);
        if (nuevoCuadro != null) {
            this.cuadros.remove(obtener(entidad));
        } else {
            throw new DAOException("No se encontró el cuadro");
        }
    }

    @Override
    public void actualizarJugador(Cuadro entidad) throws DAOException {
        Cuadro nuevoCuadro = obtener(entidad);
        if (nuevoCuadro != null) {
            this.cuadros.set(cuadros.indexOf(nuevoCuadro), entidad);
        } else {
            throw new DAOException("No se encontró el cuadro");
        }
    }

    @Override
    public Cuadro obtener(Cuadro entidad) throws DAOException {
        Iterator<Cuadro> itCuadro = this.cuadros.iterator();
        while (itCuadro.hasNext()) {
            Cuadro nuevoCuadro = itCuadro.next();
            if (nuevoCuadro.equals(entidad)) {
                return nuevoCuadro;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Cuadro> obtenerTodos() {
        return cuadros;
    }
    
    
    
}
